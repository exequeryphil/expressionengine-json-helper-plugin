<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

$plugin_info = array(
    'pi_name' => 'JSON Helper for Feeds',
    'pi_version' => '1.0',
);

class Json_helper
{

    public $return_data = "";

    public function __construct()
    {
        $this->EE =& get_instance();
        $str = $this->EE->TMPL->tagdata;
        $truncate = ($this->EE->TMPL->fetch_param('truncate')) ? $this->EE->TMPL->fetch_param('truncate') : false;
        $str = strip_tags($str);
        if ($truncate != false) {
            if (strlen($str) > 100) {
                $str = substr($str, 0, 100) . '...';
            }
        }
        $str = str_replace(["\n", "\r"], "", $str);
        $str = html_entity_decode($str);
        $encoding = mb_detect_encoding($str);
        if ($encoding !== 'UTF-8') {
            $str = mb_convert_encoding($str, 'UTF-8', $encoding);
        }
        $str = preg_replace("/[^a-z0-9. ',\/:-]+/i", " ", $str);

        $this->return_data = $str;
    }

    public static function usage()
    {
        ob_start();
        $buffer = ob_get_contents();
        ob_end_clean();
        return $buffer;
    }

}