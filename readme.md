### EE JSON Helper Plugin

This plugin is useful when building JSON feed output in EE.  It ensures the JSON won't break with dodgy input.  Best for simple strings like titles, captions, etc.

Usage is simple:

`{exp:json_helper}{untrustworthy_variable}{/exp:json_helper}`

Additionally, if you would like to truncate a potentially long string, use the truncate option:

`{exp:json_helper truncate="true"}`